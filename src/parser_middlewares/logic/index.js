const ALL_SUB = Symbol('All sub');
class ParserMiddlewares {
  static getProvider(provider) {
    if (provider) return (Array.isArray(provider) ? provider : [provider]);
    return [ALL_SUB];
  }

  static getNames(name) {
    if (name) return (Array.isArray(name) ? name : [name]);
    return [ALL_SUB];
  }

  constructor() {
    this.middlewares = [];
  }

  use(provider, name, cb) {
    if (typeof provider === 'function') return this.use(null, null, provider);
    if (typeof name === 'function') return this.use(provider, null, name);
    if (!cb || typeof cb !== 'function') throw new Error('ParserMiddlewares.use(). Callback should be function');

    const mdw = {
      providers: this.constructor.getProvider(provider),
      names: this.constructor.getNames(name),
      cb,
    };

    this.middlewares.push(mdw);

    return () => {
      this.middlewares = this.middlewares.filter(innerMdw => innerMdw !== mdw);
    };
  }

  async handle(args) {
    const { provider, name, data } = args;
    const row = this.filter(provider, name);
    if (row.length === 0) return args;

    const iterator = row[Symbol.iterator]();

    const newData = await this.iteratorWorker(iterator, data, args);
    if (!newData || typeof newData !== 'object') return null;
    return { provider, name, data: newData };
  }

  filter(provider, name) {
    return this.middlewares
      .filter(
        ({ providers, names }) => providers.includes(provider)
          || providers.includes(ALL_SUB)
          || names.includes(name)
          || names.includes(ALL_SUB),
      )
      .filter(
        ({ providers, names }) => providers.includes(ALL_SUB)
          || (providers.includes(provider) && (names.includes(name) || names.includes(ALL_SUB))),
      )
      .map(({ cb }) => cb);
  }

  async iteratorWorker(iterator, data, args) {
    const { value: cb, done } = iterator.next();
    if (done) return data;
    const newData = await cb(data, args);
    if (!newData) return null;
    return this.iteratorWorker(iterator, newData, args);
  }
}

export default ParserMiddlewares;
