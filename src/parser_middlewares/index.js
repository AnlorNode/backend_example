import ParserMiddlewares from './logic';

import mdwnest from './mdw/nest';

const middlewares = new ParserMiddlewares();

middlewares.use('NEST', mdwnest);

export default (...args) => middlewares.handle(...args);
