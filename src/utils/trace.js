/* eslint-disable no-console */
import stack from 'callsite';
import { pathToFileURL } from 'url';
import 'colors';

export default name => {
  const basePath = pathToFileURL(process.cwd()).href;
  console.log('\n Trace:'.green, name || '');
  stack()
    .filter((s, i) => i > 0 /* && s.isToplevel() */)
    .forEach((site, i) => {
      console.log(
        i > 0 ? '\t' : '',
        '-',
        site.getFileName().replace(basePath, '').green,
        site.getLineNumber(),
        '|',
        `${site.getFunctionName() || 'anonymous'}(...)`.cyan,
      );
    });
  console.log();
};
