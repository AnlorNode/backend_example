import { unlockMatch } from '@nest/queue-locker';

const unlockQueue = data => {
  // console.log(match);
  if (data.name === 'matches') {
    return unlockMatch(data.provider, data.data.id);
  }
  if (data.name === 'translations') {
    return unlockMatch(data.provider, data.data.id);
  }
  if (data.name === 'actually') {
    console.info('actually +++++++++++++++++++++++++++++++++++++++ save');
    return unlockMatch(data.provider, 'actually');
  }

  return null;
};

export default unlockQueue;
