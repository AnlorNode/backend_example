export default (validator, data) => {
  let check = false;
  if (data.name && data.name === 'matches') {
    check = validator.validateMatch(data);
  } else if (data.name && data.name === 'translations') {
    check = validator.validateTranslation(data);
  } else if (data.name && data.name === 'actually') {
    check = validator.validateActually(data);
  }
  // console.log('validator');
  if (check) {
    // console.log('passed');
  } else {
    /* console.dir(validator.report, { depth: 0 }); */
    console.error('message err', validator.report.message);
    // console.dir(validator.report.value, { depth: 0 });
    // console.log(JSON.stringify(validator.report.value.data));
    // console.log('failed');
  }
  return check;
};
