/* eslint-disable camelcase */
import moment from 'moment';
import _ from 'lodash';

export const prepare = data => {
  if (typeof data === 'object') {
    return JSON.stringify(data);
  }
  try {
    return JSON.parse(data);
  } catch (e) {
    const num = Number(data);
    if (Number.isNaN(num)) {
      return data;
    }
    return num;
  }
};
export const prepareKey = data => {
  if (typeof data === 'object') {
    return JSON.stringify(data);
  }
  return data;
};
const maxTimeOoj = {};
export const maxTime = (data, name) => {
  const dt = moment(data).toObject();
  const bufDt = new Date(
    Date.UTC(dt.years, dt.months, dt.date, dt.hours, dt.minutes, dt.seconds, dt.milliseconds),
  ).getTime() / 1000;
  if (!maxTimeOoj[name]) {
    const {
      years, months, date, hours, minutes, seconds, milliseconds,
    } = moment(
      new Date(),
    ).toObject();
    maxTimeOoj[name] = new Date(
      Date.UTC(years, months, date, hours, minutes, seconds, milliseconds),
    ).getTime() / 1000 - 100000;
  }
  if (bufDt > maxTimeOoj[name]) {
    maxTimeOoj[name] = bufDt;
  }

  return maxTimeOoj[name];
};
export const camelCaseObj = data => {
  const obj = {};
  Object.keys(data).forEach(a => {
    obj[_.camelCase(a)] = data[a];
  });
  return obj;
};
export const snakeCaseObj = data => {
  const obj = {};
  Object.keys(data).forEach(a => {
    obj[_.snakeCase(a)] = data[a];
  });
  return obj;
};
