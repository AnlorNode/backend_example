import mysql from 'mysql';
import util from 'util';
import waitPort from 'wait-port';

const {
  DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME,
} = process.env;

const sleep = t => new Promise(r => setTimeout(r, t));
const connectLoop = async (i = 200) => {
  const conn = mysql.createConnection({
    host: DB_HOST,
    port: Number(DB_PORT),
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
  });
  conn.connect = util.promisify(conn.connect);
  try {
    await conn.connect();
    conn.end();
  } catch (e) {
    console.error('Try connect db.', `Timeout: ${i}`.yellow);
    conn.end();
    await sleep(i);
    await connectLoop(parseInt(i * 1.05, 10));
  }
};

export default async () => {
  await waitPort(
    {
      host: DB_HOST,
      port: Number(DB_PORT),
      interval: 1000,
    },
    10000,
  );
  await connectLoop();
};
