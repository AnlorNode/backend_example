/* eslint-disable no-param-reassign */
import mysql from 'mysql';
import util from 'util';

const {
  DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME,
} = process.env;

const pool = mysql.createPool({
  connectionLimit: 100,
  waitForConnections: true,
  acquireTimeout: 2e4,
  host: DB_HOST,
  port: DB_PORT,
  user: DB_USER,
  charset: 'utf8mb4',
  password: DB_PASSWORD,
  database: DB_NAME,
});

pool.query = util.promisify(pool.query);

pool.queryRow = (...args) => pool.query(...args).then(r => r[0]);

export const { format } = mysql;

const createConnection = () => new Promise((resolve, reject) => {
  pool.getConnection((error, connection) => {
    if (error) return reject(error);

    connection.beginTransaction = util.promisify(connection.beginTransaction);
    connection.commit = util.promisify(connection.commit);
    connection.rollback = util.promisify(connection.rollback);
    connection.query = util.promisify(connection.query);
    connection.queryRow = async (q, p) => (await connection.query(q, p))[0];
    connection.format = (query, args) => mysql.format(query, args);

    return resolve(connection);
  });
});

pool.transaction = async transaction => {
  const connection = await createConnection();

  try {
    await connection.beginTransaction();
    await transaction(connection);
    await connection.commit();
  } catch (e) {
    await connection.rollback();

    throw e;
  } finally {
    connection.release();
  }
};

export default pool;
