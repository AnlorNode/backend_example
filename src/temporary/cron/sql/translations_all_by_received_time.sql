SELECT *
  FROM translations
  WHERE UNIX_TIMESTAMP(update_time) > ? ORDER BY update_time DESC LIMIT 10000;
