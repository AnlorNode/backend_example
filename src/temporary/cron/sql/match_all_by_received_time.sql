SELECT *
  FROM matches
  WHERE UNIX_TIMESTAMP(update_time) > ? AND service = 'LIVE' AND active = 1 ORDER BY update_time DESC LIMIT 10000;
