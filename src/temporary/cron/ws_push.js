import createAmqp from './amqp';
import buffer from '../utils/buffer';

export default async (data, taskName, { type } = {}) => {
  const { ch } = await createAmqp();
  await ch.sendToQueue(
    taskName,
    buffer({
      action: type || 'addAllData',
      data,
    }),
  );
};
