import pool from '../db/pool';
import getMatchSql from './sql/match_all_by_received_time.sql';
import getOddsSql from './sql/odds_all_by_received_time.sql';
import getTranslationsSql from './sql/translations_all_by_received_time.sql';
import pushService from './ws_push';
import { maxTime } from '../utils/conversion';
import 'colors';

const { RABBITMQ_TASKS_NAME_PUSH_SERVICE } = process.env;

export const cron = async () => {
  const match = await pool.query(getMatchSql, [maxTime(null, 'match')]);
  const odds = await pool.query(getOddsSql, [maxTime(null, 'odds')]);
  const translations = await pool.query(getTranslationsSql, [maxTime(null, 'translations')]);

  if (match[0]) {
    match.forEach(({ update_time: updateTime }) => {
      maxTime(updateTime, 'match');
    });
  }
  if (odds[0]) {
    odds.forEach(({ update_time: updateTime }) => {
      maxTime(updateTime, 'odds');
    });
  }
  if (translations[0]) {
    odds.forEach(({ update_time: updateTime }) => {
      maxTime(updateTime, 'translations');
    });
  }
  if (match.length > 0 || odds.length > 0 || translations.length > 0) {
    pushService({ odds, match, translations }, RABBITMQ_TASKS_NAME_PUSH_SERVICE);
  }

  setTimeout(cron, 2000);
};

cron();

export default { cron };
