import amqp from 'amqplib';

const {
  RABBITMQ_HOST,
  RABBITMQ_PORT,
  RABBITMQ_PROTOCOL,
  RABBITMQ_DEFAULT_USER,
  RABBITMQ_DEFAULT_PASS,
  RABBITMQ_TASKS_NEST,
} = process.env;

export const createConnection = async () => amqp.connect({
  protocol: RABBITMQ_PROTOCOL,
  hostname: RABBITMQ_HOST,
  port: RABBITMQ_PORT,
  username: RABBITMQ_DEFAULT_USER,
  password: RABBITMQ_DEFAULT_PASS,
});

export const createChannel = async taskName => {
  const connection = await createConnection();
  const ch = await connection.createChannel();
  await ch.assertQueue(taskName, { durable: true });
  return { connection, ch };
};

let currentChannel = null;

export default async () => {
  if (currentChannel) return currentChannel;
  try {
    currentChannel = await createChannel(RABBITMQ_TASKS_NEST);
    return currentChannel;
  } catch (e) {
    console.error(e);
    process.exit(1);
    return null;
  }
};
