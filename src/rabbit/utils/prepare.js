export const prepareToConsum = msg => JSON.parse(msg.content.toString());

export const prepareToSend = data => Buffer.from(JSON.stringify(data));
