import amqp from 'amqplib';
import config from '../../config';

export const createConnection = async () => amqp.connect({
  protocol: config.Rabbit.PROTOCOL,
  hostname: config.Rabbit.HOST,
  port: config.Rabbit.PORT,
  username: config.Rabbit.DEFAULT_USER,
  password: config.Rabbit.DEFAULT_PASS,
});

const channels = {};

export const createChannel = async taskName => {
  if (channels[taskName]) return channels[taskName];
  const connection = await createConnection();
  const ch = await connection.createChannel();
  await ch.assertQueue(taskName, { durable: true });
  channels[taskName] = { connection, ch };
  return channels[taskName];
};

export default createChannel;
