import { createChannel } from './connection';
import { prepareToConsum } from './utils/prepare';

import config from '../../config';

export default async cb => {
  const { ch } = await createChannel(config.Rabbit.CONSUMER_TASK_NAME);
  ch.prefetch(Number(config.Rabbit.SUB_COUNT));
  return ch.consume(config.Rabbit.CONSUMER_TASK_NAME, async msg => {
    try {
      await cb(prepareToConsum(msg));
      ch.ack(msg, false);
    } catch (e) {
      console.error(e);
      ch.nack(msg, false, false);
    }
  });
};
