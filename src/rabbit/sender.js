import { createChannel } from './connection';
import { prepareToSend } from './utils/prepare';

export default async (data, taskName) => {
  if (!data) return null;
  const { ch } = await createChannel(taskName);
  try {
    return ch.sendToQueue(taskName, prepareToSend(data));
  } catch (e) {
    console.error(e);
    return null;
  }
};
