import { MATCHES, TRANSLATIONS, ACTUALLY } from '../constants/msg_names';

export default ({ data, provider, name }, repo) => {
  const obj = { data, provider, name };

  if (!name) {
    throw Error('Name of the parser data is required');
  }


  switch (name) {
    case MATCHES:
      return repo.saveMatch(obj);
    case TRANSLATIONS:
      return repo.saveTranslation(obj);
    case ACTUALLY:
      return repo.saveActual(obj);
    default:
      throw new Error(`Handler: incorrect name - ${name}`);
  }
};
