import './main';

/*
Repo.setConnection(pool);
Repo.init();
// import parserDataSave from './controllers/parse_data';
// import './db_service/cron';

// todo remove all
// subAmqp(parserDataSave);
subAmqp(msg => {
  const message = JSON.parse(msg.content.toString());
  Repo.processMessage(message);
  // if (message.name === 'match') Repo.processMatch(message);
});

*/
/**
 * Не удалять!!!!
 * Необходимо для замеров времени выполнения
 */
// (async () => {
//   let testArr = [];

//   const consoleRows = [];
//   setInterval(() => {
//     // console.clear();

//     const count = testArr.length;

//     const getMiddle = n =>
//       count === 0
//         ? 0
//         : Math.round(testArr.reduce((p, c) => p + (Number(c.get(n)) || 0), 0) / count);

//     consoleRows.push({
//       count,
//       'all time': getMiddle('all time'),
//       odds: getMiddle('odds'),
//       matches: getMiddle('matches'),
//       translations: getMiddle('translations'),
//       oddsTypes: getMiddle('oddsTypes'),
//     });
//     if (consoleRows.length > 55) {
//       consoleRows.splice(1, consoleRows.length - 55);
//     }
//     // console.table(consoleRows);
//     testArr = [];
//   }, 10000);

//   subAmqp(async msg => {
//     const st = Date.now();
//     const curMap = new Map();
//     testArr.push(curMap);
//     await parserDataSave(msg, (n, v) => {
//       curMap.set(n, v);
//     });
//     curMap.set('all time', Date.now() - st);
//   });
// })();
