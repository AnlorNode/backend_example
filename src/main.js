import { initQueueLocker, clearLocks } from '@nest/queue-locker';
import unlockQueue from './utils/unlock_queue';
import pool from './connection/db/pool';
import sender from './rabbit/sender';
import repo from './repo';
import consumer from './rabbit/consumer';
import middleware from './parser_middlewares';
import handler from './handler';
import mapper from './mapper';
import config from '../config';

// temporary for test frontend
import './temporary/cron';

const PROVIDER_NAME = 'NEST';
const start = async () => {
  console.info('Start Applier');


  repo.setConnection(pool);

  consumer(async msg => {
    try {
      const newmsg = await middleware(msg);
      const values = await handler(newmsg, repo);
      await unlockQueue(msg);
      const msgToSend = await mapper(values, repo);
      await sender(msgToSend, config.Rabbit.RABBITMQ_TASKS_NEST);
    } catch (error) {
      await unlockQueue(msg);
      throw error;
    }
  });

  console.info('Success! start');
};

(async function main() {
  console.time('All move');
  try {
    await initQueueLocker(config.Redis.HOST, config.Redis.PORT);
    await clearLocks(PROVIDER_NAME);
    await start();
  } catch (e) {
    const err = e && e.target && e.target.lastError ? e.target.lastError : e;
    console.error(err.message.red);
    process.exit(1);
  }

  console.timeEnd('All move');
}());
