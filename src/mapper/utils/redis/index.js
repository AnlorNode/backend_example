/* eslint-disable consistent-return */
import redis from 'redis';
import util from 'util';

const { REDIS_HOST, REDIS_PORT } = process.env;

let client;

const createClient = () => {
  if (client && !client.closing) return client;
  client = redis.createClient({
    host: REDIS_HOST,
    port: Number(REDIS_PORT),
  });

  client.get = util.promisify(client.get);
  client.set = util.promisify(client.set);
  client.flushall = util.promisify(client.flushall);
  client.setMaxListeners(0);
  /* memory clearing */
  // client.flushall();
};

export const getClient = () => new Promise((resolve, reject) => {
  createClient();
  if (client.connected) return resolve(client);
  let error;

  const ready = () => {
    client.removeListener('error', error);
    resolve(client);
  };

  error = err => {
    client.removeListener('ready', ready);
    reject(err);
  };

  client.once('ready', ready);

  client.once('error', error);
});

export default { getClient };
