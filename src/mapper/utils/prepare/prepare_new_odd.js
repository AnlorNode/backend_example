import { camelCaseObj } from '../conversion';
import { prepareOdd } from './prepare_odd';

/**
 * Prepare New Odd
 * @param {object} input
 * @returns {prepareNewOdd~response}
 */
export const prepareNewOdd = input => {
  const row = camelCaseObj(input);
  return {
    type: 'add-new-odds',
    service: (row.service === 'LIVE' && 'live') || 'prematch',
    matchId: row.matchId,
    id: row.id,
    oddsTypeMap: [
      {
        subTypeId: row.group,
        typeId: row.type,
        oddsMap: [
          prepareOdd({
            coefficient: row.coefficient,
            disabled: row.disabled,
            matchId: row.matchId,
            outCome: row.outCome,
            service: row.service,
            specialValue: row.specialValue,
            blocked: row.blocked,
            group: row.group,
            type: row.type,
          }),
        ],
      },
    ],
  };
};

export default { prepareNewOdd };

/**
 * @typedef {object} prepareNewOdd~input
 */

/**
 * @typedef {object} prepareNewOdd~response
 * @property {string} type
 * @property {string} service
 * @property {*} matchId
 * @property {*} id
 * @property {object[]} oddsTypeMap
 * @property {*} oddsTypeMap.subTypeId
 * @property {*} oddsTypeMap.typeId
 * @property {prepareOdd~response} oddsTypeMap.oddsMap
 */
