/**
 * Prepare data for odd
 * @param {prepareOdd~input} input
 * @return {prepareOdd~response}
 */
export const prepareOdd = input => ({
  coefficient: String(input.coefficient),
  disabled: input.disabled,
  matchId: input.matchId,
  outCome: input.outCome || String(input.outCome),
  outComeDescription: '',
  service: (input.service === 'LIVE' && 'live') || 'prematch',
  specialValue: String(input.specialValue),
  specialValueDescription: '',
  status: input.blocked ? 1 : 0,
  subTypeId: input.group,
  typeId: input.type,
});

export default { prepareOdd };

/**
 * @typedef {object} prepareOdd~input
 * @property coefficient
 * @property disabled
 * @property matchId
 * @property outCome
 * @property service
 * @property specialValue
 * @property blocked
 * @property group
 * @property type
 */

/**
 * @typedef {object} prepareOdd~response
 * @property {string} coefficient
 * @property {*}      disabled
 * @property {*}      matchId
 * @property {string} outCome
 * @property {string} outComeDescription
 * @property {*}      service
 * @property {string} specialValue
 * @property {string} specialValueDescription
 * @property {number} status
 * @property {*}      subTypeId
 * @property {*}      typeId
 */
