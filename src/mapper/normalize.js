export const normalizeOdd = odd => ({
  coefficient: String(odd.coefficient) || 'null',
  disabled: Number(odd.disabled) || 0,
  matchId: Number(odd.matchId) || 0,
  outCome: String(odd.outCome) || '',
  outComeDescription: String(odd.outComeDescription) || '',
  service: String(odd.service) || '',
  specialValue: String(odd.specialValue) || '',
  specialValueDescription: String(odd.specialValueDescription) || '',
  status: Number(odd.status) || 0,
  subTypeId: Number(odd.subTypeId) || 0,
  typeId: Number(odd.typeId) || 0,
});

export const normalizeOddsMap = oddsMap => ({
  subTypeId: Number(oddsMap.subTypeId) || 0,
  typeId: Number(oddsMap.typeId) || 0,
  oddsMap: oddsMap.oddsMap ? oddsMap.oddsMap.map(odd => normalizeOdd(odd)) : [],
});

export const normalizeOddsUpdateProperties = input => ({
  matchId: Number(input.matchId) || 0,
  outCome: String(input.outCome) || '',
  service: String(input.service) || '',
  specialValue: String(input.specialValue) || '',
  subTypeId: Number(input.subTypeId) || 0,
  typeId: Number(input.typeId) || 0,
  updatedProperties: {
    coefficient: String(input.coefficient || input.updatedProperties.coefficient) || '',
  },
});

export const normalizeOddsUpdate = odds => {
  const updateList = [];
  const relatedMatchIds = [];
  odds.oddsTypeMap.forEach(info => {
    const item = info.oddsMap && info.oddsMap.length ? info.oddsMap[0] : {};
    const toAdd = { ...info, ...item };
    updateList.push(normalizeOddsUpdateProperties(toAdd));
  });
  updateList.forEach(info => {
    if (!relatedMatchIds.find(matchId => matchId === info.matchId)) {
      relatedMatchIds.push(info.matchId);
    }
  });
  return {
    type: 'update-odds-properties',
    service: odds.service || '',
    updateList,
    relatedMatchIds,
  };
};

export const normalizeMatch = match => ({
  active: Number(match.active) || 0,
  awayTeamId: Number(match.awayTeamId) || 0,
  awayTeamName: {
    ru: match.awayTeamName ? String(match.awayTeamName.ru) || '' : '',
    en: match.awayTeamName ? String(match.awayTeamName.en) || '' : '',
  },
  betstatus: String(match.betstatus) || '',
  categoryId: Number(match.categoryId) || 0,
  categoryName: {
    ru: match.categoryName ? String(match.categoryName.ru) || '' : '',
    en: match.categoryName ? String(match.categoryName.en) || '' : '',
  },
  dateOfMatch: Number(match.dateOfMatch) || '', // timestamp
  gameScore: String(match.gameScore) || '',
  homeTeamId: Number(match.homeTeamId) || 0,
  homeTeamName: {
    ru: match.homeTeamName ? String(match.homeTeamName.ru) || '' : '',
    en: match.homeTeamName ? String(match.homeTeamName.en) || '' : '',
  },
  matchId: Number(match.matchId),
  matchScore: String(match.matchScore),
  matchTime: Number(match.matchTime),
  oddsCount: Number(match.oddsCount),
  periodsScore: String(match.periodsScore),
  server: Number(match.server),
  service: String(match.service),
  sportId: Number(match.sportId),
  sportName: {
    ru: match.sportName ? String(match.sportName.ru) || '' : '',
    en: match.sportName ? String(match.sportName.en) || '' : '',
  },
  status: String(match.status) || '',
  totalOddsCount: Number(match.totalOddsCount) || 0,
  tournamentId: Number(match.tournamentId) || 0,
  tournamentName: {
    ru: match.tournamentName ? String(match.tournamentName.ru) || '' : '',
    en: match.tournamentName ? String(match.tournamentName.en) || '' : '',
  },
  baseOddsConfig: match.baseOddsConfig
    ? match.baseOddsConfig.map(odd => ({
      cellList: odd.cellList.map(cell => ({
        outCome: String(cell.outCome) || '',
        odd: normalizeOdd(cell),
      })),
      isSpecialValue: Boolean(odd.isSpecialValue) || false,
      oddType: {
        oddTypeName: odd.oddType ? String(odd.oddType.oddTypeName) || '' : '',
        subTypeId: odd.oddType ? Number(odd.oddType.subTypeId) || 0 : 0,
        typeId: odd.oddType ? Number(odd.oddType.typeId) || 0 : 0,
      },
    }))
    : [],
  oddsTypeMap: match.oddsTypeMap
    ? match.oddsTypeMap.map(odds => ({
      subTypeId: Number(odds.subTypeId),
      typeId: Number(odds.typeId),
    }))
    : [],
});

/**
 * Normalize Add New Odds
 * @param {object} odds
 * @returns {{oddsTypeMap: ([]), service: string | string, type: string, matchId: number}}
 */
export const normalizeAddNewOdds = odds => ({
  type: 'add-new-odds',
  service: String(odds.service) || '',
  matchId: Number(odds.matchId) || 0,
  oddsTypeMap: odds.oddsTypeMap ? odds.oddsTypeMap.map(oddsMap => normalizeOddsMap(oddsMap)) : [],
});

/**
 * Normalize Remove Match
 * @param {(string | *)} service
 * @param {(number | *)} matchId
 * @param {(number | *)} sportId
 * @returns {{sportId: number, service: string | string, type: string, matchId: number}}
 */
export const normalizeRemoveMatch = ({ service, matchId, sportId }) => ({
  type: 'remove-match',
  service: String(service) || '',
  matchId: Number(matchId) || 0,
  sportId: Number(sportId) || 0,
});

export const normalizeUpdateMatch = (match, { matchId, tempOdds }) => {
  const tempMat = {
    type: 'update-match-properties',
    matchId: Number(match.matchId) || 0,
    service: String(match.service) || '',
    updatedProperties: normalizeMatch(match.match),
  };
  if (tempOdds && tempOdds.size) {
    const { oddsTypeMap } = normalizeAddNewOdds(tempOdds.get(matchId));
    tempMat.updatedProperties.oddsTypeMap = oddsTypeMap;
  }
  return tempMat;
};

export const normalizeAddMatch = (match, { matchId, tempOdds }) => {
  const tempMat = {
    type: 'add-new-match',
    matchId: Number(match.matchId),
    service: String(match.service) || '',
    match: normalizeMatch(match.match),
  };
  if (tempOdds && tempOdds.size) {
    const { oddsTypeMap } = normalizeAddNewOdds(tempOdds.get(matchId));
    tempMat.match.oddsTypeMap = oddsTypeMap;
  }

  return tempMat;
};
