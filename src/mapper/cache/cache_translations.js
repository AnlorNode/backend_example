/* eslint-disable camelcase */
import DataLoader from 'dataloader';
import { prepare, camelCaseObj } from '../utils/conversion';
import { getClient } from '../utils/redis';
// import pool from '../db/pool';
// import getByTranslations from './sql/get_by_translations.sql';

const { REDIS_CACHE_TIME } = process.env;
export default translations => new DataLoader(data => Promise.all(
  data.map(
    keys => new Promise(async (resolve, reject) => {
      try {
        const {
          id, provider, type, lang,
        } = prepare(keys);
        const client = await getClient();
        let dat = await client.get(`_applier_${keys}`);
        if (!dat) {
          // dat = await pool.queryRow(getByTranslations, [provider, id, type, lang]);
          dat = await translations({
            provider, id, type, lang,
          });
          if (dat) {
            await client.set(`_applier_${keys}`, JSON.stringify(dat), 'EX', REDIS_CACHE_TIME);
            return resolve([camelCaseObj(dat)]);
          }
          return resolve([null]);
        }
        const res = [camelCaseObj(prepare(dat))];
        return resolve(res);
      } catch (error) {
        return reject(error);
      }
    }),
  ),
));
