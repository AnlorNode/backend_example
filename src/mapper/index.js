import match from './match';
import validator from './send_validator';

import {
  normalizeOddsUpdate,
  normalizeAddNewOdds,
  normalizeRemoveMatch,
  normalizeUpdateMatch,
  normalizeAddMatch,
} from './normalize';

export default async (data, repo) => {
  if (!data) return null;
  const { total: newMatches, prev: oldMatches, provider } = data;
  const translations = repo.getTranslation;

  let res;
  let tempMatch;
  let tempOdds;
  const tempOldMatches = new Map();
  const tempOldOdds = new Map();
  const addNewOdds = [];
  const updateOddsProperties = [];
  if (newMatches) {
    const { addMatch, addOdds } = await match(newMatches, { provider, translations });
    tempMatch = addMatch;
    tempOdds = addOdds;
  }
  if (oldMatches) {
    tempOldMatches.set(oldMatches.id, oldMatches);
  }
  if (oldMatches.odds) {
    oldMatches.odds.forEach(od => {
      tempOldOdds.set(od.matchId, od);
    });
  }

  if (tempOdds) {
    tempOdds.forEach((tempOdd, i) => {
      const { id, matchId } = tempOdd;
      if (tempOldOdds && tempOldOdds.has(id)) {
        updateOddsProperties.push(normalizeOddsUpdate(tempOdds.get(i)));
      } else {
        addNewOdds.push(normalizeAddNewOdds(tempOdds.get(matchId)));
      }
    });
  }
  if (tempMatch) {
    const {
      match: { sportId, matchId, service },
    } = tempMatch;

    if (tempOldMatches.has(matchId)) {
      if (newMatches.status === 'remove') {
        res = normalizeRemoveMatch({
          service,
          matchId,
          sportId,
        });
      } else {
        res = normalizeUpdateMatch(tempMatch, { matchId, tempOdds });
      }
    } else {
      res = normalizeAddMatch(tempMatch, { matchId, tempOdds });
    }
  }
  validator(res);
  return res;
};
