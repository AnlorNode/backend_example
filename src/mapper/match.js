import { prepareMatch } from './prepare';
import { prepareOdd } from './utils/prepare/prepare_odd';
import { prepareNewOdd } from './utils/prepare/prepare_new_odd';
import { camelCaseObj } from './utils/conversion';

export default async (newMatches, { provider, translations }) => {
  const newMatchesOne = await prepareMatch(newMatches, { provider, translations });
  const addMatch = newMatchesOne;
  const addOdds = new Map();
  if (newMatches.odds && Object.keys(newMatches.odds).length > 0) {
    newMatches.odds.forEach(input => {
      const {
        matchId,
        coefficient,
        disabled,
        outCome,
        service,
        specialValue,
        blocked,
        group,
        type,
      } = camelCaseObj(input);
      if (!addOdds.has(matchId)) {
        addOdds.set(matchId, prepareNewOdd(input));
      } else {
        const arr = addOdds.get(matchId).oddsTypeMap;
        let tempKey = false;
        arr.forEach((elMap, i) => {
          if (elMap.typeId === type) {
            arr[i].oddsMap.push(
              prepareOdd({
                coefficient,
                disabled,
                matchId,
                outCome,
                service,
                specialValue,
                blocked,
                group,
                type,
              }),
            );
            tempKey = true;
          }
        });
        if (!tempKey) {
          addOdds.get(matchId).oddsTypeMap.push({
            subTypeId: group,
            typeId: type,
            oddsMap: [
              prepareOdd({
                coefficient,
                disabled,
                matchId,
                outCome,
                service,
                specialValue,
                blocked,
                group,
                type,
              }),
            ],
          });
        }
      }
    });
  }
  return { addMatch, addOdds };
};
