import _ from 'lodash';
import cacheTranslations from './cache/cache_translations';
import { camelCaseObj, prepare, prepareKey } from './utils/conversion';

const {
  TRANSLATIONS_SPORT,
  TRANSLATIONS_CATEGORY,
  TRANSLATIONS_TURNIR,
  TRANSLATIONS_TEAM,
} = process.env;

const str = d => _.get(d, '[0].value', '');

export const prepareMatch = async (input, { provider, translations }) => {
  const row = camelCaseObj(input);
  return {
    type: 'add-new-match',
    matchId: row.id,
    service: (row.service === 'LIVE' && 'live') || 'prematch',
    match: {
      active: row.active,
      awayTeamId: row.awayTeamId,
      awayTeamName: {
        ru: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.awayTeamId,
              type: TRANSLATIONS_TEAM,
              lang: 'ru',
              provider,
            }),
          ),
        ),
        en: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.awayTeamId,
              type: TRANSLATIONS_TEAM,
              lang: 'en',
              provider,
            }),
          ),
        ),
      },
      baseOddsConfig: [],
      betstatus: row.betstatus || '',
      categoryId: row.categoryId || 0,
      categoryName: {
        ru: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.categoryId || 0,
              type: TRANSLATIONS_CATEGORY,
              lang: 'ru',
              provider,
            }),
          ),
        ),
        en: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.categoryId || 0,
              type: TRANSLATIONS_CATEGORY,
              lang: 'en',
              provider,
            }),
          ),
        ),
      },
      dateOfMatch: row.dateOfMatch,
      gameScore: prepareKey(row.gameScore),
      homeTeamId: row.homeTeamId || 0,
      homeTeamName: {
        ru: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.homeTeamId || 0,
              type: TRANSLATIONS_TEAM,
              lang: 'ru',
              provider,
            }),
          ),
        ),
        en: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.homeTeamId || 0,
              type: TRANSLATIONS_TEAM,
              lang: 'en',
              provider,
            }),
          ),
        ),
      },
      matchId: row.id,
      matchScore: prepareKey(row.matchScore),
      matchTime: row.matchTime || 0,
      oddsCount: 0,
      oddsTypeMap: [],
      periodsScore: prepare(row.periodsScore),
      server: Number(row.activeTeam) || 0,
      service: (row.service === 'LIVE' && 'live') || 'prematch',
      sportId: row.sportId,
      sportName: {
        ru: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.sportId,
              type: TRANSLATIONS_SPORT,
              lang: 'ru',
              provider,
            }),
          ),
        ),
        en: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.sportId,
              type: TRANSLATIONS_SPORT,
              lang: 'en',
              provider,
            }),
          ),
        ),
      },
      status: row.status,
      totalOddsCount: 0,
      tournamentId: row.tournamentId,
      tournamentName: {
        ru: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.tournamentId,
              type: TRANSLATIONS_TURNIR,
              lang: 'ru',
              provider,
            }),
          ),
        ),
        en: str(
          await cacheTranslations(translations).load(
            prepareKey({
              id: row.tournamentId,
              type: TRANSLATIONS_TURNIR,
              lang: 'en',
              provider,
            }),
          ),
        ),
      },
    },
  };
};

export default { prepareMatch };
