import yup from 'yup';

import { reqNumberIntegerPositive, reqTypeString, serviceEnum } from './constants';

export const schemaRemoveMatch = yup.object().shape({
  matchId: reqNumberIntegerPositive,
  service: serviceEnum,
  type: reqTypeString,
  sportId: reqNumberIntegerPositive,
});


export default { schemaRemoveMatch };
