import yup from 'yup';

import { odd } from './odd';

import { reqNumberIntegerPositive } from './constants';

export const oddsType = yup.object().shape({
  oddsMap: yup
    .array()
    .of(odd)
    .ensure(),
  subTypeId: reqNumberIntegerPositive,
  typeId: reqNumberIntegerPositive,
});

export default { oddsType };
