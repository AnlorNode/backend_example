export const serviceList = ['prematch', 'live'];

export const typeList = [
  'update-odds-properties',
  'add-new-odds',
  'remove-match',
  'update-match-properties',
  'add-new-match',
];
