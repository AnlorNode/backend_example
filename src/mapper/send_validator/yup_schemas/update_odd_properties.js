import yup from 'yup';

import {
  reqNumber, reqNumberIntegerPositive, serviceEnum, ensureString,
} from './constants';

export const oddUpdateProperties = yup.object().shape({
  matchId: reqNumberIntegerPositive,
  outCome: ensureString,
  service: serviceEnum,
  specialValue: ensureString,
  subTypeId: reqNumber,
  typeId: reqNumberIntegerPositive,
  updatedProperties: yup.object().shape({
    coefficient: ensureString,
  }),
});

export default { oddUpdateProperties };
