import yup from 'yup';

import { typeList, serviceList } from './enum';

export const reqNumber = yup.number().required();

export const reqString = yup.string().required();

export const nullableString = yup.string().nullable();

export const ensureString = yup.string().ensure();

export const reqNumberIntegerPositive = yup
  .number()
  .required()
  .positive()
  .integer();

export const reqTypeString = yup
  .string()
  .required()
  .ensure()
  .oneOf(typeList);

export const serviceEnum = yup
  .string()
  .required()
  .ensure()
  .oneOf(serviceList);
