import yup from 'yup';

import { matchSchema } from './match';

import { reqNumberIntegerPositive, reqTypeString, serviceEnum } from './constants';

export const schemaUpdateMatch = yup.object().shape({
  matchId: reqNumberIntegerPositive,
  service: serviceEnum,
  type: reqTypeString,
  updatedProperties: matchSchema,
});

export default { schemaUpdateMatch };
