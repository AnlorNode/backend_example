import yup from 'yup';

import { matchSchema } from './match';

import { reqNumberIntegerPositive, reqTypeString, serviceEnum } from './constants';

export const schemaNewMatch = yup.object().shape({
  matchId: reqNumberIntegerPositive,
  service: serviceEnum,
  type: reqTypeString,
  match: matchSchema,
});

export default { schemaNewMatch };
