import yup from 'yup';

import { odd } from './odd';

import { reqNumber, reqString, ensureString } from './constants';

const outCome = yup.object().shape({
  outCome: ensureString,
  odd,
});

export const baseOddsConfig = yup.object().shape({
  cellList: yup.array().of(outCome),
  isSpecialValue: yup.boolean(),
  oddType: yup.object().shape({
    oddTypeName: reqString,
    subTypeId: reqNumber,
    typeId: reqNumber,
  }),
});


export default { baseOddsConfig };
