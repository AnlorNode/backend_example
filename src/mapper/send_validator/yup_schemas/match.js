import yup from 'yup';

import { baseOddsConfig } from './base_odds_config';
import { oddsType } from './odds_type';
import { gameScore, periodsScore } from './scores';

import {
  reqNumberIntegerPositive,
  serviceEnum,
  reqNumber,
  reqString,
  ensureString,
  nullableString,
} from './constants';

export const matchSchema = yup.object().shape({
  active: reqNumber,
  awayTeamName: yup
    .object()
    .shape({
      en: nullableString,
      ru: nullableString,
    })
    .required(),
  baseOddsConfig: yup.array().of(baseOddsConfig),
  betstatus: ensureString,
  categoryId: reqNumber,
  categoryName: yup
    .object()
    .shape({
      en: nullableString,
      ru: nullableString,
    })
    .required(),
  dateOfMatch: reqNumber,
  gameScore,
  homeTeamId: reqNumber,
  homeTeamName: yup
    .object()
    .shape({
      en: nullableString,
      ru: nullableString,
    })
    .required(),
  matchId: reqNumberIntegerPositive,
  matchScore: gameScore,
  matchTime: reqNumber,
  oddsCount: reqNumber,
  oddsTypeMap: yup
    .array()
    .of(oddsType)
    .ensure(),
  periodsScore,
  server: reqNumber,
  service: serviceEnum,
  sportId: reqNumberIntegerPositive,
  sportName: yup.object().shape({
    en: nullableString,
    ru: nullableString,
  }),
  status: reqString,
  totalOddsCount: reqNumber,
  tournamentId: reqNumber,
  tournamentName: yup.object().shape({
    en: nullableString,
    ru: nullableString,
  }),
});

export default { matchSchema };
