import yup from 'yup';

import { oddsType } from './odds_type';

import { reqNumberIntegerPositive, reqTypeString, serviceEnum } from './constants';

export const schemaNewOdd = yup.object().shape({
  matchId: reqNumberIntegerPositive,
  service: serviceEnum,
  type: reqTypeString,
  oddsTypeMap: yup.array().of(oddsType),
});

export default { schemaNewOdd };
