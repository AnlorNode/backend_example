import yup from 'yup';

import {
  reqNumberIntegerPositive, serviceEnum, reqString, ensureString,
} from './constants';

export const odd = yup.object().shape({
  coefficient: yup.string().nullable(),
  disabled: yup.number(),
  matchId: reqNumberIntegerPositive,
  outCome: reqString,
  outComeDescription: ensureString,
  service: serviceEnum,
  specialValue: yup.string(),
  specialValueDescription: ensureString,
  status: yup.number(),
  subTypeId: reqNumberIntegerPositive,
  typeId: reqNumberIntegerPositive,
});

export default { odd };
