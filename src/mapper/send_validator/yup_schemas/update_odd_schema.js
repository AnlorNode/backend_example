import yup from 'yup';

import { oddUpdateProperties } from './update_odd_properties';

import { reqTypeString, serviceEnum } from './constants';

export const schemaUpdateOdd = yup.object().shape({
  service: serviceEnum,
  type: reqTypeString,
  updateList: yup.array().of(oddUpdateProperties),
  relatedMatchIds: yup.array().of(yup.number()),
});

export default { schemaUpdateOdd };
