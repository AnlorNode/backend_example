import { schemaNewMatch } from './yup_schemas/new_match_schema';
import { schemaUpdateMatch } from './yup_schemas/update_match_schema';
import { schemaRemoveMatch } from './yup_schemas/remove_match_schema';
import { schemaNewOdd } from './yup_schemas/new_odd_schema';
import { schemaUpdateOdd } from './yup_schemas/update_odd_schema';

const yupConfig = {
  strict: true,
  abortEarly: false,
  stripUnknown: true,
  recursive: true,
};

const result = {
  type: '',
  valid: false,
  errors: null,
};

const methods = {
  'update-match-properties': obj => schemaUpdateMatch.validateSync(obj, yupConfig),
  'add-new-match': obj => schemaNewMatch.validateSync(obj, yupConfig),
  'remove-match': obj => schemaRemoveMatch.validateSync(obj, yupConfig),
  'add-new-odds': obj => schemaNewOdd.validateSync(obj, yupConfig),
  'update-odds-properties': obj => schemaUpdateOdd.validateSync(obj, yupConfig),
};

export default (obj, showError = true) => {
  try {
    if (obj && obj.type) {
      result.type = obj.type;
      result.valid = true;
      methods[obj.type.toString()](obj);
    }
    return result;
  } catch (e) {
    result.type = e.value && e.value.type ? e.value.type : '';
    if (result.type) {
      result.errors = e;
      if (e && e.name && e.errors && showError) {
        if (result.type === 'add-new-odds' || result.type === 'update-odds-properties') {
          result.errors.errors = result.errors.errors.map((item, index) => {
            let res = item;
            const inner = result.errors.inner[index] || null;
            if (inner && typeof inner.value !== 'undefined') {
              res = `${res} ${typeof inner.value}`;
            }
            return res;
          });
        }
        // console.info(`${result.type} ${e.name}`, e.errors);
      }
    }
    return result;
  }
};
