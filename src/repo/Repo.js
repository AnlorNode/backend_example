/* eslint-disable no-underscore-dangle */
import MatchModel from './models/match/MatchModel';
import TranslationModel from './models/translation/TranslationModel';
import { logger, poolStorage } from './utils';

class Repo {
  constructor() {
    this.connection = null;
    this.logger = logger;

    this._errorProcessor = (key, input) => reason => {
      this.logger.error(`--------------${key}------------`);
      this.logger.error(input);
      this.logger.error('------------------------------------------');
      this.logger.error(reason);
      this.logger.error('==========================================');
    };
  }

  setConnection(connection) {
    this.connection = connection;
    poolStorage.setConnection(connection);
    this.logger.info('Connection set');
    return this;
  }

  /**
   * Process match update and actualize
   * @param message
   * @returns {Promise<{prev: Match, total: Match, provider: string, live: string}>}
   */
  saveMatch(message) {
    this.logger.info(
      `[${message.data.id}] -> receiver from parser (status: ${message.data.status})`,
    );
    return MatchModel.updateMatchWithOdds(message.data, {
      provider: message.provider,
      live: 'live',
    }).catch(this._errorProcessor('save match', message.data));
  }

  /**
   * Process actual for Tournaments and matches
   * @param {ActualMessage} message
   */
  async saveActual(message) {
    this.logger.info('Saving actual', message.data);
    const { data, provider } = message;
    const toProcess = [];

    if (data.tournaments) {
      toProcess.push(
        MatchModel.actualizeMatchesBig(data.tournaments, { provider, live: 'live' }).catch(
          this._errorProcessor('save actual tournaments', data.tournaments),
        ),
      );
    }
    if (data.sports) {
      toProcess.push(
        MatchModel.actualizeTournamentsSmall(data.sports, { provider, live: 'live' }).catch(
          this._errorProcessor('save actual sports', data.sports),
        ),
      );
    }

    const [actualMatches] = await Promise.all(toProcess);
    return actualMatches;
  }

  /**
   * Process Translations
   * @param {object} message
   */
  async saveTranslation({
    provider, data: {
      id, type, lang, value,
    },
  }) {
    await TranslationModel.addTraslation(
      provider,
      id,
      type,
      lang,
      value,
    );
    return null;
  }

  /**
   * Get translations
   * @param {Repo.saveTranslation~params}params
   * @returns {Promise<Array<object>>}
   */
  getTranslation(params) {
    return TranslationModel.getTranslations(params)[0];
  }
}

export default new Repo();

/**
 * @typedef {{
 *   provider: string,
 *   id: string,
 *   lang: string
 * }} Repo.saveTranslation~params
 * @property {string} provider - provider id
 * @property {string} id - translation id
 * @property {string} lang - translation lang
 */
