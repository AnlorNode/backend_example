import METHODS from './methods';

export { default as METHODS } from './methods';

export default {
  METHODS,
};
