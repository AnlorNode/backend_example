import Dataloader from 'dataloader';
import BaseModel from '../BaseModel';
import { updateOddListPrepare } from './components';
import updateOddListSql from './sql/update_odd_list.sql';
import disabledOddsSql from './sql/disabled_odds.sql';

export default class OddsLoaders {
  constructor({ maxOddsForChunkSave, maxOddsForChunkDisable }) {
    /**
     * Максимальное количество исходов которое можно отправить в базу за раз.
     */
    this.maxOddsForChunkSave = maxOddsForChunkSave || 500;
    this.maxOddsForChunkDisable = maxOddsForChunkDisable || this.maxOddsForChunkSave || 500;

    this.updateLoaders = {};
    this.disableLoaders = {};
  }

  getUpdateLoader(provider, service) {
    const key = `${provider}_${service}`;
    if (!this.updateLoaders[key]) {
      this.updateLoaders[key] = this.createUpdateLoader(provider, service);
    }

    return this.updateLoaders[key];
  }

  getDisableLoader(provider) {
    if (!this.disableLoaders[provider]) {
      this.disableLoaders[provider] = this.createDisableLoader(provider);
    }
    return this.disableLoaders[provider];
  }

  createUpdateLoader(provider, service) {
    return new Dataloader(async odds => {
      const preparedOdds = updateOddListPrepare(odds, provider, service);
      const queryResult = await BaseModel.connection.query(updateOddListSql, [preparedOdds]);
      return odds.map(odd => ({ ...odd, queryResult }));
    }, {
      cache: false,
      cacheKeyFn: odd => odd.id,
      maxBatchSize: this.maxOddsForChunkSave,
    });
  }

  createDisableLoader(provider) {
    return new Dataloader(async oddIds => {
      const queryResult = await BaseModel.connection.query(disabledOddsSql, [oddIds, provider]);
      return oddIds.map(id => ({ id, queryResult }));
    }, {
      cache: false,
      maxBatchSize: this.maxOddsForChunkDisable,
    });
  }
}
