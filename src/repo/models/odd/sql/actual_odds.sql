UPDATE odds 
    SET `disabled`=IF(odds.id IN (?), 0, 1 ) 
    WHERE provider_id=? AND match_id=?;
