/* eslint-disable camelcase */

export default (odds, provider, service) => odds.map(
  ({
    id,
    match_id,
    group,
    type,
    out_come,
    special_value,
    player,
    coefficient,
    disabled,
    meta,
    status,
  }) => [
    id,
    provider,
    service,
    match_id,
    group,
    type,
    out_come,
    special_value,
    player,
    coefficient,
    disabled,
    JSON.stringify(meta),
    status,
  ],
);
