import poolStorage from '../../../utils/poolStorage';
import sql from '../sql/actual_odds.sql';

/**
 * Actual odds small
 * @param {{ids: Array<number>, matchId: number}} params
 * @param {{provider: string}} options
 */
export const actualOddSmall = (params, options) => (
  poolStorage.connection.query(sql, [params.ids, options.provider, params.matchId])
);

export default actualOddSmall;
