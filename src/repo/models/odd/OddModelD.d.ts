import { Odd } from '../../../../types/odd';

declare type OddModelDUpdateParams = {
  prev: Array<Odd>;
  total: Array<Odd>;
};

declare type OddModelDUpdateOptions = {
  provider: string;
  live: string;
  actual?: boolean;
};

declare type OddModelDUpdateResponse = {
  prev: Array<Odd>;
  total: Array<Odd>;
  provider: string;
  live: string;
};

declare type OddModelDUpdateOddParam = {};
