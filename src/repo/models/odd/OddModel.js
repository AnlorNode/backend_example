import merge from 'merge';
import BaseModel from '../BaseModel';
import { REMOVE, CHANGED } from '../../../constants/status';
import { actualOddSmall } from './components';
import { snakecase, logger } from '../../utils';
import OddLoaders from './loaders';


const oddLoaders = new OddLoaders({ maxOddsForChunkSave: 500, maxOddsForChunkDisable: 1000 });

class OddModel extends BaseModel {
  static get table() {
    return 'odds';
  }

  get table() {
    return OddModel.table;
  }

  /**
   * Update odds list
   * @param {Array<Odd>} odds
   * @param {MatchModelUMOptions} options
   * @returns {Promise<void>}
   */
  updateOddsList(odds, { provider, live }) {
    if (odds.length === 0) return null;

    const loader = oddLoaders.getUpdateLoader(provider, this.getService(live));

    try {
      return Promise.all(odds.map(odd => loader.load(odd)));
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  /**
   * Disable odds list
   * @param {Array<Odd | OddDb>}odds
   * @param {{provider: string}} options
   */
  disableOddsList(odds, { provider }) {
    const loader = oddLoaders.getDisableLoader(provider);
    try {
      return Promise.all(odds.map(odd => loader.load(odd.id)));
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  /**
   * Is odd disabled
   * @param {OddDb} odd
   * @param {OddDb | {}} [prevOdd]
   * @returns {number}
   */
  isOddDisabled(odd, prevOdd) {
    if (prevOdd && prevOdd.disabled) return 1;
    return odd.status === REMOVE || odd.status === CHANGED ? 1 : 0;
  }

  /**
   * Parse and get odd meta
   * @param {Odd | OddDb | {}}  odd
   * @returns {object}
   */
  getOddMeta(odd) {
    try {
      return JSON.parse(odd.meta);
    } catch (e) {
      return {};
    }
  }

  /**
   * Actual odds
   * @param {OddModelDUpdateParams}params
   * @param {OddModelDUpdateOptions} options
   */
  async actualOdd(params, options) {
    const { total, prev } = params;
    const { provider, live } = options;
    const matchId = total.length ? total[0].matchId : undefined;

    const mapPrevOdds = new Map();
    const mapTotalOdds = new Map();

    prev.forEach(odd => mapPrevOdds.set(odd.id, odd));
    total.forEach(odd => mapTotalOdds.set(odd.id, odd));

    const toDisable = prev.map(snakecase).filter(odd => !mapTotalOdds.has(odd.id))
      .map(odd => ({ ...odd, disabled: 1 }));
    const toUpdate = total.map(snakecase)/* .filter(odd => !mapPrevOdds.has(odd.id)) */
      .map(odd => ({
        ...odd,
        disabled: this.isOddDisabled(odd),
        status: odd.blocked ? 1 : 0,
      }));


    logger.debug(`[${matchId}] -> actualize match odds (toDisable: ${toDisable.length} toUpdate: ${toUpdate.length})`);

    await Promise.all([
      this.updateOddsList(toUpdate, { provider, live }),
      this.disableOddsList(toDisable, { provider }),
    ]);

    return { prev, total };
  }

  /**
   * Actual odds small
   * @param {{odds: Array<Odd | OddDb>, matchId: number}}params
   * @param {{provider: string}} options
   * @returns {Promise<void>}
   */
  async actualOddSmall(params, options) {
    const { odds, matchId } = params;
    const { provider } = options;
    return actualOddSmall(
      {
        ids: odds.map(odd => odd.id),
        matchId,
      },
      { provider },
    );
  }

  /**
   * Update odds
   * @param {OddModelDUpdateParams} params
   * @param {OddModelDUpdateOptions} options
   * @returns {Promise<OddModelDUpdateResponse>}
   */
  async updateOdds(params, { provider, live, actual = false }) {
    if (actual) return this.actualOdd(params, { provider, live });
    const { total, prev } = params;
    if (!total || !total.length) return null;
    const odds = total;
    const mapPrevOdds = new Map();
    prev.forEach(odd => mapPrevOdds.set(odd.id, odd));
    const toUpdate = odds.map(snakecase).map(odd => {
      const { id, meta } = odd;
      const prevOdd = mapPrevOdds.get(id) || {};
      const newOdd = merge({}, prevOdd, odd);
      newOdd.meta = merge.recursive({}, prevOdd.meta, meta);
      newOdd.id = id;
      newOdd.status = odd.blocked ? 1 : 0;
      newOdd.status = prevOdd && prevOdd.status === 1 ? prevOdd.status : newOdd.status;
      newOdd.disabled = this.isOddDisabled(odd, prevOdd);
      return newOdd;
    });

    logger.debug(
      '[ODDS] -> Odds to change',
      toUpdate.map(odd => ({
        id: odd.id,
        disabled: odd.disabled,
        status: odd.status,
      })),
    );

    await this.updateOddsList(toUpdate, { provider, live });
    return {
      prev,
      total: toUpdate,
    };
  }
}

export default new OddModel();
