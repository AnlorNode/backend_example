import Dataloder from 'dataloader';
import BaseModel from '../BaseModel';
import insertTranslationsSql from './sql/insert.sql';
import getTranslationsByIdsSql from './sql/get_translations_by_ids.sql';


class TranslationModel extends BaseModel {
  constructor() {
    super();
    this.loaderMapGetter = {};
    this.loaderInsert = this.createLoaderInsert();
  }

  addTraslation(provider, id, type, lang, value) {
    return this.loaderInsert.load({
      id, type, lang, value, provider,
    });
  }

  /**
   * Get Translations array by id provider and lang
   * @param {TranslationModel#getTranslations~params} params
   * @returns {Promise<Array<object>>}
   */

  getTranslations({
    id, type, lang, provider,
  }) {
    if (id === undefined || !type) throw new Error(`One of the options is missing. [id=${id}, type=${type}]`);
    const loader = this.getLoaderGetter(provider, lang);
    return loader.load({ id, type });
  }

  getLoaderGetter(provider, lang) {
    if (!provider || !lang) throw new Error(`One of the options is missing. [provider=${provider}, lang=${lang}]`);

    if (!this.loaderMapGetter[provider]) {
      this.loaderMapGetter[provider] = {};
    }

    if (!this.loaderMapGetter[provider][lang]) {
      this.loaderMapGetter[provider][lang] = this.createLoaderGetter(provider, lang);
    }

    return this.loaderMapGetter[provider][lang];
  }

  createLoaderGetter(provider, lang) {
    return new Dataloder(async arrTranslations => {
      const ids = [];
      const types = [];
      arrTranslations.forEach(({ id, type }) => {
        if (!ids.includes(id)) ids.push(id);
        if (!types.includes(type)) types.push(type);
      });

      const inserts = [provider, lang, types, ids, ids.length];

      const res = await BaseModel.connection.query(getTranslationsByIdsSql, inserts);

      return arrTranslations.map(({ id, type }) => res.find(
        translation => translation.id === id && translation.type === type,
      ) || null);
    }, {
      cache: false,
      cacheKeyFn: ({ id, type }) => `${type}_${id}`,
    });
  }


  createLoaderInsert() {
    return new Dataloder(async arrTranslations => {
      const inserts = arrTranslations.map(({
        id, type, lang, value, provider,
      }) => [id, type, lang, value, provider]);

      const res = await this.connection.query(insertTranslationsSql, [inserts]);

      return arrTranslations.map(translation => ({ ...translation, ...res }));
    }, {
      cache: false,
      cacheKeyFn: ({
        id, type, lang, value, provider,
      }) => [id, type, lang, value, provider].join('_'),
    });
  }
}

export default new TranslationModel();


/**
 * @typedef {{
 *   provider: string,
 *   lang: string,
 *   id: string,
 * }} TranslationModel#getTranslations~params
 * @property {string} provider - provider id
 * @property {string} lang - lang
 * @property {string} id - id
 */
