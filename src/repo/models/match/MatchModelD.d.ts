import '../../../../types/score';
import '../../../../types/period_score';
import '../../../../types/stats';

declare type MatchModelUMOptions = {
  provider: string;
  live: string | boolean;
  small?: boolean;
};

declare type MatchDb = {
  id: number;
  actual: boolean;
  active: number;
  finished: boolean;
  blocked: boolean;
  tournament_id: number;
  sport_id: number;
  homeTeam_id: number;
  awayTeam_id: number;
  date_of_match: number;
  result_received_time: number;
  match_time: number;
  status: string;
  match_score: string | Score | {} | null;
  game_score: string | Score | {} | null;
  periods_score: string | PeriodScore | null;
  meta: string | object | null;
  odds: Array<OddDb>;
};

declare type OddDb = {
  coefficient: number;
  group: number;
  type: number;
  match_id: number;
  special_value: number;
  out_come: string | null;
  ACT: number;
  meta: string | object | null;
  id: string;
  status: 1 | 0;
  disabled: 1 | 0;
};

declare type BasicOptions = {
  provider: string;
  live: string;
};
