UPDATE
    matches
SET
    `active`=0
WHERE
    provider_id=?
    AND `active`=1
    AND `service`=?
    AND sport_id IN (?)
    AND tournament_id NOT IN (?);
