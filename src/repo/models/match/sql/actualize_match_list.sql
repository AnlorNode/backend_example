UPDATE
    matches
SET
    `active`=IF(matches.id IN (?), 1, 0)
WHERE provider_id = ?
  AND `service` = ?
  AND tournament_id IN (?);
