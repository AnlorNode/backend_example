SELECT
    matches.*,
    odds.id as odds_id,
    odds.provider_id as odds_provider_id,
    odds.service as odds_service,
    odds.match_id as odds_match_id,
    odds.`group` as odds_group,
    odds.type as odds_type,
    odds.update_time as odds_update_time,
    odds.out_come as odds_out_come,
    odds.special_value as odds_special_value,
    odds.player as odds_player,
    odds.coefficient as odds_coefficient,
    odds.disabled as odds_disabled,
    odds.meta as odds_meta,
    odds.status as odds_status
FROM
    matches JOIN odds ON odds.match_id = matches.id
WHERE
    matches.id = ? AND odds.provider_id = ? ;

