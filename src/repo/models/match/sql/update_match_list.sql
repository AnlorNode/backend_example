REPLACE INTO matches
    (id, provider_id, `service`, tournament_id, category_id, sport_id, home_team_id, away_team_id, date_of_match, active, match_time, match_score, periods_score, game_score, active_team, result_received_time, status )
    VALUES ?;
