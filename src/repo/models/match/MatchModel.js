import merge from 'merge';
import Dataloader from 'dataloader';
import BaseModel from '../BaseModel';
import {
  getScores,
  getMatchesAndOdds,
  updateMatchListPrepare,
  actualizeTournaments,
  actualizeMatches,
  getMatchesByIdTournamentId,
  updateActiveMatch,
} from './components';

import updateMatchListSql from './sql/update_match_list.sql';
import {
  camelCaseObj, snakecase, getService, logger,
} from '../../utils';
import { INIT, REMOVE } from '../../../constants/status';
import MatchMetaModel from '../match_meta';
import OddModel from '../odd';

class MatchModel extends BaseModel {
  constructor() {
    super();

    this.updateMatchLoader = this.createUpdateMatchLoader();
  }

  static get table() {
    return 'matches';
  }

  get table() {
    return MatchModel.table;
  }

  /**
   * [Primitive] Get match with his odds
   * @param {number} id
   * @param {string} provider
   * @returns {Promise<MatchDb>}
   */
  getMatchWithOdds(id, provider) {
    return getMatchesAndOdds(id, provider);
  }

  /**
   * [Primitive] Actualize tournaments
   * @param {Map | Array<Sports>}params
   * @param {{provider: string, live: string}} options
   * @returns {Promise<void>}
   */
  actualizeTournaments(params, options) {
    const { provider, live } = options;
    const preparedOptions = { provider, service: getService(live) };
    if (params instanceof Map) {
      return actualizeTournaments(params, preparedOptions);
    }
    return actualizeTournaments(new Map(params), preparedOptions);
  }

  /**
   * [Primitive] Actualize Matches
   * @param {Map | Array<Tournaments>}params
   * @param {{provider: string, live: string}} options
   * @returns {Promise<void>}
   */
  actualizeMatches(params, options) {
    const { provider, live } = options;
    const preparedOptions = { provider, service: getService(live) };
    if (params instanceof Map) {
      return actualizeMatches(params, preparedOptions);
    }
    return actualizeMatches(new Map(params), preparedOptions);
  }

  /**
   * Get match status
   * @param {MatchDb | Match | object} prev
   * @param {MatchDb | Match} total
   * @returns {string}
   */
  getMatchStatus(prev, total) {
    if (!prev.status && total.status !== REMOVE) {
      return INIT;
    }
    return prev.status && prev.status === REMOVE ? REMOVE : total.status;
  }

  /**
   * Is match active ?
   * @param {MatchDb | Match} match
   * @param {MatchDb} prev
   * @returns {number}
   */
  static isActive(match, prev) {
    if (!prev.active && prev.id) return 0;
    return match.status !== REMOVE ? 1 : 0;
  }

  /**
   * Update match list
   * @param {Array<MatchDb>} matches
   * @param {{provider: string, live: string}} options
   * @returns {Promise<void>}
   */
  async updateMatchList(matches, options) {
    const preparedMatches = updateMatchListPrepare(matches, options.provider, getService(options.live));

    return Promise.all(preparedMatches.map(match => this.updateMatchLoader.load(match)));
  }

  /**
   * Update match
   * @param {Match} match
   * @param {MatchModelUMOptions} options
   * @returns {{prev: Match, total: Match, provider: string, live: string}}
   */
  async updateMatchWithOdds(match, { provider, live, small = false }) {
    if (match.actual && small) return this.actualizeMatchWithOddsSmall(match, { provider });

    if (match.actual && !small) return this.actualizeMatchWithOdds(match, { provider, live });

    const metaData = [{ id: match.id, ...snakecase(match.meta) }];
    const prev = (await this.getMatchWithOdds(match.id, provider)) || { odds: [] };
    const { matchScore, gameScore, periodsScore } = getScores(prev);
    prev.game_score = gameScore;
    prev.match_score = matchScore;
    prev.periods_score = periodsScore;

    const mergeMatch = merge.recursive({}, prev, snakecase(match));
    mergeMatch.id = match.id;
    mergeMatch.status = this.getMatchStatus(prev, match);
    mergeMatch.active = mergeMatch.status === REMOVE ? 0 : 1;
    if (mergeMatch.periods_score.length === 0 && periodsScore.length > 0) {
      mergeMatch.periods_score = periodsScore;
    }
    delete mergeMatch.odds;
    logger.debug(
      `[${mergeMatch.id}] -> save to database (status: ${mergeMatch.status} actual: ${mergeMatch.actual})`,
    );
    const [, , oddsResponse] = await Promise.all([
      MatchMetaModel.updateMeta(metaData, { provider }),
      this.updateMatchList([mergeMatch], { provider, live }),
      OddModel.updateOdds({ prev: prev.odds, total: match.odds }, { provider, live }),
    ]);

    if (oddsResponse !== null) {
      mergeMatch.odds = oddsResponse.total;
    } else {
      mergeMatch.odds = [];
    }

    return {
      prev: camelCaseObj(prev),
      total: camelCaseObj(mergeMatch),
      provider,
      live,
    };
  }

  /**
   * Actualize odds without prev
   * @param {Match | MatchDb} match
   * @param {{provider: string}} options
   * @returns {Promise<void>}
   */
  async actualizeMatchWithOddsSmall(match, options) {
    return OddModel.actualOddSmall({ odds: match.odds, matchId: match.id }, options);
  }

  /**
   * Actualize match and odds
   * @param {Match} match
   * @param {MatchModelUMOptions} options
   * @returns {{prev: Match, total: Match, provider: string, live: string}}
   */
  async actualizeMatchWithOdds(match, options) {
    const { provider, live } = options;
    const prev = (await this.getMatchWithOdds(match.id, provider)) || { odds: [] };
    const { matchScore, gameScore, periodsScore } = getScores(prev);
    prev.game_score = gameScore;
    prev.match_score = matchScore;
    prev.periods_score = periodsScore;
    // const metaData = [{ id: match.id, ...snakecase(match.meta) }];
    logger.debug(
      `[${match.id}] -> Actualize odds (total: ${match.odds.length} prev: ${prev.odds.length})`,
    );
    const mergeMatch = merge.recursive({}, prev, snakecase(match));
    mergeMatch.id = match.id;
    mergeMatch.status = match.status;
    mergeMatch.active = match.status === REMOVE ? 0 : 1;
    if (mergeMatch.periods_score.length === 0 && periodsScore.length > 0) {
      mergeMatch.periods_score = periodsScore;
    }

    delete mergeMatch.odds;
    logger.debug(
      // eslint-disable-next-line max-len
      `[${mergeMatch.id}] -> Save to database when actualize odds (status: ${mergeMatch.status} actual: ${mergeMatch.actual})`,
    );
    const [, , oddsResponse] = await Promise.all([
    //  MatchMetaModel.updateMeta(metaData, { provider }),
      this.updateMatchList([mergeMatch], { provider, live }),

      OddModel.updateOdds({ prev: prev.odds, total: match.odds }, { provider, live, actual: true }),
    ]);

    mergeMatch.odds = oddsResponse ? oddsResponse.total || {} : {};
    return {
      prev: camelCaseObj(prev),
      total: camelCaseObj(mergeMatch),
      provider,
      live,
    };
  }

  /**
   * Actualize Matches Active by tournaments
   * @param {Array<Tournaments>} tournaments
   * @param {BasicOptions} options
   * @returns {Promise<void>}
   */
  async actualizeMatchesSmall(tournaments, options) {
    return this.actualizeMatches(tournaments, options);
  }

  /**
   * Actualize Tournaments Active by sports
   * @param {Array<Sports>} sports
   * @param {BasicOptions} options
   * @returns {Promise<void>}
   */
  async actualizeTournamentsSmall(sports, options) {
    return this.actualizeTournaments(sports, options);
  }

  /**
   * Actualize matchers using tournaments relations
   * With returning old & new data
   * @param {Array<Tournaments>} tournaments
   * @param {BasicOptions} options
   * @returns {Promise<void>}
   */
  async actualizeMatchesBig(tournaments, options) {
    const toUpdate = [];
    const matches = await getMatchesByIdTournamentId({
      provider: options.provider,
      tournamentIds: tournaments.map(t => t[0]),
    });

    tournaments.forEach(([tournamentId, enabledIds]) => {
      const matchesInArray = matches.filter(
        match => Number(match.tournament_id) === Number(tournamentId),
      );
      if (!matchesInArray.length) {
        logger.warn('Empty matches in tournament', tournamentId);
        return;
      }
      matchesInArray.forEach(match => {
        const active = !!enabledIds.find(id => Number(id) === Number(match.id));
        if (Number(match.active) !== Number(active)) {
          toUpdate.push({
            ...match,
            active: Number(active),
          });
        }
      });
    });
    toUpdate.forEach(match => {
      logger.debug(`[${match.id}] -> Actualization action (actual: ${match.active})`);
    });

    await updateActiveMatch({ matches: toUpdate, provider: options.provider });
    return {
      prev: matches.filter(match => toUpdate.find(toUpdateMatch => toUpdateMatch.id === match.id)),
      total: toUpdate,
    };
  }

  /**
   * Actualize matchers using sports relations
   * With returning old & new data
   * @param {Array<Sports>} sports
   * @param {BasicOptions} options
   * @returns {Promise<void>}
   */
  async actualizeTournamentsBig(sports, options) {
    return this.actualizeTournaments(sports, options);
  }

  createUpdateMatchLoader() {
    return new Dataloader(async matches => {
      const res = await this.connection.query(updateMatchListSql, [matches]);
      return matches.map(match => ({ ...match, queryResult: res }));
    }, {
      cache: false,
      cacheKeyFn: ({ provider, id }) => `${provider}_${id}`,
    });
  }
}

export default new MatchModel();
