import poolStorage from '../../../utils/poolStorage';
import actualizeMatchListSql from '../sql/actualize_match_list.sql';

/**
 * Actualize Matches with sports
 * @param {Map} mapT
 * @param {{provider: string, service: string}} options
 * @returns {Promise<void>}
 */
export const actualizeMatches = async (mapT, options) => {
  const { provider, service } = options;
  const inserts = [[].concat(...mapT.values()), provider, service, [...mapT.keys()]];
  return poolStorage.connection.query(actualizeMatchListSql, inserts);
};

export default actualizeMatches;
