import poolStorage from '../../../utils/poolStorage';
import actualizeTournamentsSql from '../sql/actualize_tournaments.sql';

/**
 *
 * @param {Map} mapS
 * @param {string} provider
 * @param {string} service
 * @param connection
 * @returns {Promise<void>}
 */
export const actualizeTournaments = async (mapS, { provider, service }) => {
  mapS.forEach((value, key) => (!value.length ? mapS.set(key, [-1]) : null));
  const inserts = [provider, service, [...mapS.keys()], [].concat(...mapS.values())];
  await poolStorage.connection.query(actualizeTournamentsSql, inserts);
};

export default actualizeTournaments;
