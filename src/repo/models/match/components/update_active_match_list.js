import { poolStorage } from '../../../utils';
import sql from '../sql/update_matches_active_list.sql';

/**
 * @param {UpdateActiveMatchParams} params
 * @returns {Promise<Array<*>>}
 */
export const updateActiveMatch = params => {
  const toActive = params.matches.filter(match => match.active);
  const toDisabled = params.matches.filter(match => !match.active);
  return Promise.all([
    toActive.length
      ? poolStorage.connection.query(sql, [1, 'update', params.provider, toActive.map(match => match.id)])
      : [],
    toDisabled.length
      ? poolStorage.connection.query(sql, [0, 'remove', params.provider, toDisabled.map(match => match.id)])
      : [],
  ]);
};

export default updateActiveMatch;

/**
 * @typedef UpdateActiveMatchParams
 * @type {{
 *   matches: Match || MatchDb,
 *   provider: string,
 * }}
 * @property {Match || MatchDb} matches
 * @property {string} provider
 */
