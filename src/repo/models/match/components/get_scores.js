/**
 * Parse json
 * @param {string} string
 * @param {{array: boolean}} [options]
 * @returns {{} | object}
 * @private
 */
const parseJOSN = (string, options = { array: false }) => {
  try {
    return JSON.parse(string);
  } catch (e) {
    return options.array ? [] : {};
  }
};

/**
 * Parse score from json
 * @param {MatchDb | {}} match - match
 * @returns {getScoresResponse}
 */
export const getScores = match => ({
  matchScore: parseJOSN(match.match_score),
  gameScore: parseJOSN(match.game_score),
  periodsScore: parseJOSN(match.periods_score, { array: true }),
});

export default getScores;
/**
 * @typedef {{
 *   matchScore: Score | {},
 *   gameScore: Score | {},
 *   periodsScore: Array<PeriodScore> || []
 * }} getScoresResponse
 */
