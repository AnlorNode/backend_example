/* eslint-disable camelcase */

/**
 * Update Match list prepare
 * @param {Array<MatchDb>} matches
 * @param {string} provider
 * @param {string} service
 */

export const updateMatchListPrepare = (matches, provider, service) => matches.map(({
  id,
  tournament_id,
  category_id,
  sport_id,
  home_team_id,
  away_team_id,
  date_of_match,
  active,
  match_time,
  match_score,
  periods_score,
  game_score,
  active_team,
  result_received_time,
  status,
}) => [
  id,
  provider,
  service,
  tournament_id || 0,
  category_id || 0,
  sport_id || 0,
  Number(home_team_id) || 0,
  Number(away_team_id) || 0,
  date_of_match,
  active,
  match_time,
  JSON.stringify(match_score),
  JSON.stringify(periods_score),
  JSON.stringify(game_score),
  active_team,
  result_received_time,
  status,
]);
