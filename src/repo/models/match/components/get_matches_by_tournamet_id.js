import { poolStorage } from '../../../utils';
import sql from '../sql/get_matches_by_tournamet_id.sql';

/**
 *
 * @param {getMatchesByIdTournamentIdParams} params
 * @returns {Promise<Array<MatchDb>>}
 */
export const getMatchesByIdTournamentId = params => (
  poolStorage.connection.query(sql, [params.provider, params.tournamentIds])
);

export default getMatchesByIdTournamentId;

/**
 * @typedef {{provider: string, tournamentIds: Array<number>}} getMatchesByIdTournamentIdParams
 * @property {string} provider
 * @property {string} tournamentIds
 */
