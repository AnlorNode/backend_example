import Dataloader from 'dataloader';
import poolStorage from '../../../utils/poolStorage';
import getMatchesByIdsSql from '../sql/get_matches_by_ids.sql';
import getOddsByMatchIdsSql from '../sql/get_odds_by_match_ids.sql';


const providerCacheLoaders = {};

const createLoaders = provider => {
  const connection = poolStorage.getConnection();
  const matchLoader = new Dataloader(async matchIds => {
    const res = await connection.query(getMatchesByIdsSql, [provider, matchIds, matchIds.length]);
    return matchIds.map(id => res.find(match => match.id === id) || null);
  }, {
    cache: false,
  });

  const oddLoader = new Dataloader(async matchIds => {
    const res = await connection.query(getOddsByMatchIdsSql, [provider, matchIds]);

    const oddsArrs = {};

    res.forEach(odd => {
      if (!oddsArrs[odd.match_id]) {
        oddsArrs[odd.match_id] = [];
      }
      oddsArrs[odd.match_id].push(odd);
    });

    return matchIds.map(id => oddsArrs[id] || []);
  }, {
    cache: false,
  });

  return { matchLoader, oddLoader };
};

const getLoaders = provider => {
  if (!providerCacheLoaders[provider]) {
    providerCacheLoaders[provider] = createLoaders(provider);
  }
  return providerCacheLoaders[provider];
};


/**
 * @param {number} id
 * @param {string} provider
 * @returns {Promise<MatchDb | null>}
 */
export const getMatchesAndOdds = async (id, provider) => {
  const { matchLoader, oddLoader } = getLoaders(provider);

  const [rawMatch, odds] = await Promise.all([
    matchLoader.load(id),
    oddLoader.load(id),
  ]);

  if (!rawMatch || rawMatch.length === 0) return null;

  const match = rawMatch || {};
  match.odds = odds;

  return match;
};


export default getMatchesAndOdds;
