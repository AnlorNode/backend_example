import poolStorage from '../utils/poolStorage';
import getService from '../../utils/get_service';

class BaseModel {
  static get table() {
    throw new Error('Missed table');
  }

  static get connection() {
    return poolStorage.connection;
  }

  get connection() {
    return this.constructor.connection;
  }

  /**
   * Get service
   * @param {string} type
   * @returns {string}
   */
  static getService(type) {
    return getService(type);
  }

  getService(type) {
    return getService(type);
  }
}

export default BaseModel;
