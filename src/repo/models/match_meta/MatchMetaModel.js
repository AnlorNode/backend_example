import BaseModel from '../BaseModel';
import { setMatchMeta } from './components/meta';

class MatchMetaModel extends BaseModel {
  static get table() {
    return 'matches_meta';
  }

  /**
   * Update match meta
   * @param {*} meta
   * @param {{provider: string}} options
   * @returns {Promise<undefined>}
   */
  static updateMeta(meta, { provider }) {
    return setMatchMeta(meta, { provider });
  }
}

export default MatchMetaModel;
