/* eslint-disable camelcase */
import { logger, poolStorage } from '../../../utils';
import addMetaSql from '../sql/add_meta.sql';

const getMetaScheme = arr => {
  const res = [];
  arr.forEach(({ id, ...m }) => {
    Object.keys(m).forEach(key => {
      let value = m[key];
      if (typeof value === 'object') {
        value = JSON.stringify(value);
      }
      res.push({
        match_id: id,
        key,
        value,
      });
    });
  });
  return res;
};

export const setMatchMeta = async (meta, { provider }) => {
  if (meta.length === 0) return;
  const nArr = getMetaScheme(meta);
  if (nArr.length === 0) return;
  const matchMetaArr = nArr.map(({ match_id, key, value }) => {
    let tmp = value;
    if (value.length > 10000) {
      logger.warn('[SET META] setMatchMeta: Too long match meta data value');
      tmp = value.substring(0, 10000);
    }
    return [match_id, provider, key, tmp];
  });
  await poolStorage.connection.query(addMetaSql, [matchMetaArr]);
};

export default setMatchMeta;
