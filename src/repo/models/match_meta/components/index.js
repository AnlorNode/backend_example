import { setMatchMeta } from './meta';

export { setMatchMeta } from './meta';
export default {
  setMatchMeta,
};
