export const mapToArrayOfValues = map => [...map].map(items => items[1]);

export default mapToArrayOfValues;
