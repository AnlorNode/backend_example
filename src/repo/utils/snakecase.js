/* eslint-disable no-param-reassign */
import lodashSnakecase from 'lodash.snakecase';

export default data => {
  if (typeof data === 'string') return lodashSnakecase(data);
  if (!data || typeof data !== 'object') return null;
  return Object.keys(data).reduce((prev, key) => {
    prev[lodashSnakecase(key)] = data[key];
    return prev;
  }, {});
};
