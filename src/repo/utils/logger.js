import log4js from 'log4js';

log4js.configure({
  appenders: {
    repo: {
      type: 'dateFile',
      filename: './logs/repo.log',
      pattern: '.hh-mm-dd-MM-yyyy',
      compress: true,
    },
    console: {
      type: 'console',
    },
  },
  categories: { default: { appenders: ['repo'], level: 'all' } },
});

const logger = log4js.getLogger('REPO');
logger.level = 'all';

export default logger;
