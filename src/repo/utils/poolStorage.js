class PoolStorage {
  constructor() {
    this.storedConnection = null;
  }

  setConnection(connection) {
    this.storedConnection = connection;
    return this;
  }

  getConnection() {
    return this.storedConnection;
  }

  get connection() {
    return this.storedConnection;
  }
}

export default new PoolStorage();
