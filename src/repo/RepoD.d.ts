import { Match } from '../../types/match';
import '../../types/tournaments';
import '../../types/sports';

type MatchMessage = {
  name: 'match';
  provider: string;
  data: Match;
};

declare type ActualMessage = {
  name: 'actually';
  provider: string;
  data: {
    tournaments?: Array<Tournaments>;
    sports?: Array<Sports>;
  };
};

declare type RepoMessage = MatchMessage | ActualMessage;
