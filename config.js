export default {
  Rabbit: {
    PROTOCOL: process.env.RABBITMQ_PROTOCOL,
    HOST: process.env.RABBITMQ_HOST,
    PORT: Number(process.env.RABBITMQ_PORT),
    DEFAULT_USER: process.env.RABBITMQ_DEFAULT_USER,
    DEFAULT_PASS: process.env.RABBITMQ_DEFAULT_PASS,
    CONSUMER_TASK_NAME: process.env.RABBIT_CONSUMER_TASK_NAME,
    SENDER_TASK_NAME: process.env.RABBIT_SENDER_TASK_NAME,
    SUB_COUNT: process.env.RABBIT_SUB_COUNT || 1,
    RABBITMQ_TASKS_NEST: process.env.RABBITMQ_TASKS_NEST,
  },

  Redis: {
    HOST: process.env.REDIS_HOST,
    PORT: process.env.REDIS_PORT,
  },

  MySql: {
    HOST: process.env.DB_HOST,
    PORT: process.env.DB_PORT,
    USER: process.env.DB_USER,
    PASSWORD: process.env.DB_PASSWORD,
    DB_NAME: process.env.DB_NAME,
  },
};
