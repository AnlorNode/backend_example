import path from 'path';
import fs from 'fs';

export async function dynamicInstantiate(url) {
  const p = new URL(url);
  return {
    exports: ['default'],
    execute: exports => {
      const data = fs.readFileSync(p.pathname);
      exports.default.set(data.toString());
    },
  };
}

export function resolve(specifier, parentModuleURL, defaultResolver) {
  if (path.extname(specifier) === '.sql') {
    return {
      url: path.join(parentModuleURL ? path.dirname(parentModuleURL) : process.cwd(), specifier),
      format: 'dynamic',
    };
  }
  const resolvedModule = defaultResolver(specifier, parentModuleURL);

  return resolvedModule;
}
