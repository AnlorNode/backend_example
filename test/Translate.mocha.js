import './utils/config';
import { generateTranslation } from './utils/generators';
import RabbitMq from './utils/RabbitMq';

const Mq = new RabbitMq();

describe('Translate test', () => {
  it('Init rabbit', () => Mq.init());
  it('Send translate', () => Mq.send(generateTranslation()));
  it('Close connection', () => Mq.close());
});
