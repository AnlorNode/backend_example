import assert from 'assert';
import { RabbitMq, sleep } from './utils';
import { deleteMatch, writeMatch, getMatch } from './utils/db';
import { METHODS } from '../src/repo/constansts';

const Mq = new RabbitMq();

describe('Actualize matches by sports', () => {
  before(() => {
    return Promise.all([Mq.init()]);
  });
  beforeEach(() => deleteMatch({ id: 0xff }));

  it('Disabled by actualize', async () => {
    await writeMatch({ active: true });
    const sports = [[0xff, []]];
    await Mq.send({ name: METHODS.ACTUAL, provider: 'NEST', data: { sports } });
    await sleep(1e3);
    const [match] = await getMatch({ id: 0xff });
    assert.ok(!!match, 'Match not found');
    assert.ok(!match.active, 'Match still active');
    await deleteMatch({ id: match.id });
  });

  it('Enabled by actualize', async () => {
    await writeMatch({ active: true });
    const sports = [[0xff, [0xff]]];
    await Mq.send({ name: METHODS.ACTUAL, provider: 'NEST', data: { sports } });
    await sleep(1e3);
    const [match] = await getMatch({ id: 0xff });
    assert.ok(!!match, 'Match not found');
    assert.ok(!!match.active, 'Match non active active');
    await deleteMatch({ id: match.id });
  });

  after(() => {
    return deleteMatch({ id: 0xff }).then(() => Promise.all([Mq.close()]));
  });
});
