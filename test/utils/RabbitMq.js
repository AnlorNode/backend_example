import mq from 'amqplib';

const { RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_TASKS_NAME } = process.env;

class RabbitMq {
  /**
   * From object to buffer
   * @param {*} data
   * @returns {Buffer}
   */
  static prepareToSend(data) {
    return Buffer.from(JSON.stringify(data));
  }

  /**
   * Delay
   * @param {number} ms
   * @returns {Promise<unknown>}
   */
  static sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  constructor() {
    this.connection = null;
    this.channel = null;
  }

  /**
   * Init connection and changel
   * @returns {Promise<void>}
   */
  async init() {
    await this._connect();
    await this._assertQuery(RABBITMQ_TASKS_NAME);
  }

  /**
   * Connection to rabbit
   * @returns {Promise<{channel: null, connection: null}>}
   * @private
   */
  async _connect() {
    this.connection = await mq.connect(`amqp://${RABBITMQ_HOST}:${RABBITMQ_PORT}`);
    this.channel = await this.connection.createChannel();
    return { connection: this.connection, channel: this.channel };
  }

  /**
   * Listen to query
   * @param name
   * @returns {Promise<*|{durable, ticket, autoDelete, exclusive, arguments, passive, queue, nowait}>}
   * @private
   */
  async _assertQuery(name) {
    return this.channel.assertQueue(name);
  }

  /**
   * Send data
   * @param {*} data
   * @returns {Promise<void>}
   */
  async send(data) {
    this.channel.sendToQueue(RABBITMQ_TASKS_NAME, this.constructor.prepareToSend(data));
  }

  /**
   * Close connection
   * @returns {Promise<void>}
   */
  async close() {
    await this.constructor.sleep(100);
    await this.connection.close();
  }
}

export default RabbitMq;
