function* idGenerator() {
  yield 'custom:1';
  yield 'custom:2';
  yield 'custom:3';
  yield 'custom:4';
  yield 'custom:5';
  return 'custom:6';
}

export const generator = () => {
  class Generator {
    constructor() {
      this.gen = idGenerator();
    }

    next() {
      return this.gen.next().value;
    }
  }

  return new Generator();
};
