/**
 * @returns {Score}
 */
export const generateScore = () => ({
  Sc1: 0,
  Sc2: 0,
  Title: 'Time 1',
});
