function* id() {
  yield 'custom:1';
  yield 'custom:2';
  yield 'custom:3';
  yield 'custom:4';
  yield 'custom:5';
  return 'custom:6';
}

const generator = id();

/**
 *
 * @param {{deleted?: boolean}} [options = {deleted: false}]
 * @returns {Odd}
 */
export const generateOdd = (options = { deleted: false }) => ({
  coefficient: 0,
  group: 1,
  type: 1,
  matchId: 10,
  specialValue: '10',
  outCome: '10',
  ACT: 0,
  blocked: false,
  meta: {},
  id: generator.next().value,
  status: options.deleted ? 'remove' : 'add',
});
