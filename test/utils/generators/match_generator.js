import { generateScore } from './score_generator';
import { generateOdd } from './odd_generator';
import { generateMatchMeta } from './match_meta_generator';

/**
 * @param {{actual?: boolean}} [options={}]
 * @returns {{name: string, provider: string, data: Match}}
 */
export const generateMatch = (options = {}) => ({
  name: 'match',
  provider: 'nest',
  data: {
    id: 10,
    actual: !!options.actual,
    finished: false,
    blocked: false,
    tournamentId: 10,
    sportId: 1,
    homeTeamId: 10,
    awayTeamId: 10,
    dateOfMatch: Date.now(),
    resultReceivedTime: Date.now(),
    matchTime: 60 * 60,
    status: 'add',
    matchScore: generateScore(),
    gameScore: generateScore(),
    periodsScore: [generateScore()],
    meta: generateMatchMeta(),
    odds: [generateOdd(), generateOdd({ deleted: false })],
  },
});
