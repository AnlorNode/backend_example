import connection from './connection';

export const deleteMatch = params => {
  return connection.query(`DELETE FROM matches WHERE id = ? AND provider_id = 'NEST'`, [
    params.id,
  ]);
};
