import connection from './connection';

export const getMatch = parmas => {
  return connection.query(
    `SELECT *
                           FROM matches
                           WHERE id = ?
                             and provider_id = ?`,
    [parmas.id, 'NEST'],
  );
};
