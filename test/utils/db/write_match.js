import connection from './connection';

export const writeMatch = (options = { active: true }) => {
  const sqlParams = [
    ['id', 0xff],
    ['provider_id', 'NEST'],
    ['service', 'LIVE'],
    ['active', options.active ? 1 : 0],
    ['status', 1],
    ['tournament_id', 0xff],
    ['sport_id', 0xff],
    ['home_team_id', 0xff],
    ['away_team_id', 0xff],
  ];

  const sql = `
      INSERT INTO matches
      (
       id,
       provider_id,
       service,
       active,
       status,
       tournament_id,
       sport_id,
       home_team_id,
       away_team_id
      )
      VALUES (?)
  `;
  return connection.query(sql, [sqlParams.map(s => s[1])]).catch(reason => {
    console.error(reason.sql);
    return Promise.reject(reason);
  });
};
