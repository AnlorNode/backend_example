import './utils/config';
import { generateMatch } from './utils/generators';
import { RabbitMq } from './utils';

const Mq = new RabbitMq();

describe.skip('Main test', () => {
  it('Init rabbit', () => Mq.init());
  it('Send match', () => Mq.send(generateMatch({ actual: true })));
  it('Close connection', () => Mq.close());
});
