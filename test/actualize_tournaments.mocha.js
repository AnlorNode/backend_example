import assert from 'assert';
import { METHODS } from '../src/repo/constansts';
import { writeMatch, getMatch, deleteMatch } from './utils/db';
import { sleep, RabbitMq } from './utils';

const Mq = new RabbitMq();

describe.skip('Actualize Tournaments', () => {
  before(() => Promise.all([Mq.init()]));
  it('Connect to mq', () => Mq.init());

  describe('Test deactive by tournaments', () => {
    before(() => deleteMatch({ id: 0xff }));
    it('Create non actual match records', () => writeMatch());
    it('Actual match records by tournaments [disable current]', async () => {
      const tournaments = [[0xff, []], [1931396, [206142369]]];
      await Mq.send({ name: METHODS.ACTUAL, provider: 'NEST', data: { tournaments } });
    });
    it('Check actual match record', async () => {
      await sleep(1e3);
      const [match] = await getMatch({ id: 0xff });
      if (!match) throw new Error('Record was deleted');
      assert.ok(!match.active, 'Match still active');
    });
  });
  describe('Test active by tournaments', () => {
    before(() => deleteMatch({ id: 0xff }));
    it('Create non actual match records', () => writeMatch({ active: false }));
    it('Actual match records by tournaments [disable current]', async () => {
      const tournaments = [[0xff, [0xff]], [1931396, [206142369]]];
      await Mq.send({ name: METHODS.ACTUAL, provider: 'NEST', data: { tournaments } });
    });
    it('Check actual match record', async () => {
      await sleep(1e3);
      const [match] = await getMatch({ id: 0xff });
      if (!match) throw new Error('Record was deleted');
      assert.ok(!!match.active, 'Match still active');
    });
  });
  after(() => {
    it('Clear data', async () => {
      const [match] = await getMatch({ id: 0xff });
      if (!match) return null;
      await deleteMatch({ id: match.id });
    });
    it('Close connection', () => Promise.all([Mq.close()]));
  });
});
