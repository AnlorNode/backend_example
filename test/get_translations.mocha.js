import assert from 'assert';
import './utils/config';
import Repo from '../src/repo/Repo';
import { connection } from './utils/db';

describe('Get translation test', () => {
  it('Init repo', () => Repo.setConnection(connection));
  it('Return translation', async () => {
    const translations = await connection.query('SELECT * FROM translations LIMIT 1');
    const transaction = await Repo.getTranslation({
      provider: translations[0].provider_id,
      id: translations[0].id,
      lang: translations[0].lang,
    });

    assert.deepStrictEqual(translations[0], transaction);
  });
});
