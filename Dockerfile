FROM node:12 
WORKDIR /app
RUN printf '@nest:registry=http://fbet-gitlab.ex2b.co:4873/\n//fbet-gitlab.ex2b.co:4873/:_authToken="LNQ2AYp7C5OLh7H0kO0VLg=="\n' > .npmrc

COPY package*.json ./
RUN npm i
COPY scripts/ scripts/
COPY src/ src/
COPY .env.example ./.env
